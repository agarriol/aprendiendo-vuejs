import {mapActions} from 'vuex';
import router from 'js/router.js';
import template from './index.pug';

export default Vue.extend({
  template: template(),
  data() {
    return {
      errors: [],
      email: null,
      password: null,
      token: localStorage.getItem('pruebaSession')
    };
  },
  methods: {
    ...mapActions([
      'saveUser'// mapea this.saveUser() to this.$store.dispatch('saveUser')
    ]),
    checkForm: function checkForm(e) {
      if (!this.email || !this.password) {
        this.errors = [];
        if (!this.email) this.errors.push('Email required.');
        if (!this.password) this.errors.push('Password required.');
        e.preventDefault();
      } else {
        this.sendLogin();
      }
      return true;
    },
    sendLogin: function sendLogin() {
      API.user.session({email: this.email, password: this.password}).then(response => {
        localStorage.setItem('pruebaSession', response.session.jwt); // Guardar response en LocalStorage, por ahora mostrarlo
        this.$store.dispatch('saveUser', {jwt: response.session.jwt, email: response.session.email});
      });
      router.push('/');
    }
  }
});
