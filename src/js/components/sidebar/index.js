import {mapGetters} from 'vuex';
import {mapActions} from 'vuex';
import {mapState} from 'vuex';
import router from 'js/router.js';
import template from './index.pug';

export default Vue.extend({
  template: template(),
  data() {
    return {
      users: {
        userToken: this.$store.state.users.userToken,
        userEmail: this.$store.state.users.userEmail
      },
      usersasdf: '',
      session: this.$store.getters.loginUser// localStorage.getItem('pruebaSession')
    };
  },
  methods: {
    ...mapActions([
      'saveUser', // mapea this.saveUser() to this.$store.dispatch('saveUser')
      'deleteUser'
    ]),
    loginOut: function loginOut() {
      localStorage.clear();
      this.session = '';
      this.$store.dispatch('deleteUser');
      // location.reload();
      router.go(0);
    },
  },
  computed: {
    ...mapGetters([
      'loginUser'
    ])
  },
  created: function () {
    this.users = this.$store.getters.loginUser;
    // this.users = this.$store.state.users;
  }
});
