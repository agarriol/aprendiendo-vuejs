export default {
  index(params, queryParams = '') {
    return Vue.http.get('http://localhost:3000/comments'+queryParams, params).then(response => {
      response.data.data.status = response.status;
      return response.data.data;
    }, response => {
      return response;
    });
  },

  show(id, params) {
    return Vue.http.get(`http://localhost:3000/comments/${id}`, params).then(response => {
      return response.data.data;
    });
  },

  create(data, params) {
    const sendData = data;// instanceof FormData ? data : {data};

    return Vue.http.post('http://localhost:3000/comments', sendData, params).then(response => {
      response.data.data.status = response.status;
      return response.data.data;
    }, response => {
      return response;
    });
  },

  update(id, data, params) {
    const sendData = data;// instanceof FormData ? data : {data};
    console.log(sendData);
    return Vue.http.put(`http://localhost:3000/comments/${id}`, sendData, params).then(response => {
      return response.data.data;
    });
  },

  destroy(id, params) {
    return Vue.http.delete(`http://localhost:3000/comments/${id}`, params).catch(() => {
      return Promise.reject(Vue.i18n.t('general.error'));
    });
  }
};
