export default {
  index(params) {
    return Vue.http.get('http://localhost:3000/users', params).then(response => {
      return response.data.data;
    });
  },

  show(id, params) {
    return Vue.http.get(`http://localhost:3000/users/${id}`, params).then(response => {
      return response.data.data;
    });
  },

  create(data) {
    // data: email y password
    const sendData = data instanceof FormData ? data : {data};

    return Vue.http.post('http://localhost:3000/registration', sendData).then(response => {
      return response.data.data;
    });
  },

  update(id, data) {
    const sendData = data instanceof FormData ? data : {data};

    return Vue.http.put(`http://localhost:3000/users/${id}`, sendData).then(response => {
      return response.data.data;
    });
  }, // No se si funcia UPDATE con los usuarios

  destroy(id) {
    return Vue.http.delete(`http://localhost:3000/users/${id}`).catch(() => {
      return Promise.reject(Vue.i18n.t('general.error'));
    });
  },

  session(data) {
    // Data: email y password
    const sendData = data; //instanceof FormData ? session : {data};
    //console.log(sendData);
    return Vue.http.post('http://localhost:3000/session', sendData).then(response => {
      return response.data;
    });
  },

  deleteSession(data) {
    // Sin data. Token como cabecera
    const sendData = data instanceof FormData ? data : {data};

    return Vue.http.post('http://localhost:3000/registration', sendData).then(response => {
      return response.data.data;
    });
  }
};
