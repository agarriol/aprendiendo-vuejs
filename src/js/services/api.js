import user from './api/user';
import micropost from './api/micropost';
import comment from './api/comment';

export default{
  user,
  micropost,
  comment
};
