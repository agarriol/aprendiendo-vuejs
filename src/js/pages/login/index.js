import ListaComponent from 'js/components/login/index.js';

import template from './index.pug';

export default Vue.extend({
  template: template(),
  components: {
    ListaComponent
  },
  data() {
    return {
    };
  },
  methods: {
  }
});
