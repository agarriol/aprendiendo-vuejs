import router from 'js/router.js';
import template from './create.pug';

export default Vue.extend({
  template: template(),
  components: {
  },
  data() {
    return {
      id: this.$route.params.id,
      microposts: [],
      errors: [],
      content: '',
      title: '',
      file: '',
      titleHasError: false,
      contentHasError: false
    };
  },
  methods: {
    checkForm: function checkForm(e) {
      const formData = new FormData();
      formData.append('micropost[picture]', this.file);
      formData.append('micropost[content]', this.content);
      formData.append('micropost[title]', this.title);
      this.sendPost(formData);
      // this.sendPost();
      return true;
    },
    sendPost: function sendPost(data) {
      API.micropost.create(data, {headers: {Authorization: 'Bearer '+localStorage.getItem('pruebaSession')}}).then(response => {
        if (response.status === 422) {
          this.varErrorReset();
          this.errors.push(response);
          for (let i = 0; i < response.data.errors.length; i++) {
            this.errors.push(response.data.errors[i]);
            if (response.data.errors[i].title === 'Invalid title') {
              this.titleHasError = true;
            }
            if (response.data.errors[i].title === 'Invalid content') {
              this.contentHasError = true;
            }
          }
          this.contentHasError = true;
        } else {
          this.varReset();
        }
      });
      // location.href ="http://localhost:8080";
      router.push('/');
    },
    processFile(event) {
      this.file = event.target.files[0];
    },
    varReset() {
      this.content = '';
      this.title = '';
      this.file = '';
      this.varErrorReset();
    },
    varErrorReset() {
      this.errors = [];
      this.titleHasError = false;
      this.contentHasError = false;
    }
  }
});