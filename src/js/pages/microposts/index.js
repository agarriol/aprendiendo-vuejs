import router from 'js/router.js';
import template from './index.pug';

export default Vue.extend({
  template: template(),
  components: {

  },
  data() {
    return {
      microposts: []
    };
  },
  methods: {
    deleteMicropost: function deleteMicropost(id) {
      let r = confirm('¿Quiere borrar el post?');
      if (r === true) {
        API.micropost.destroy(id, {headers: {Authorization: 'Bearer '+localStorage.getItem('pruebaSession')}}).then(response => {
        });
        router.go(0);
      }
    }
  },
  created: function () {
    API.micropost.index({headers: {Authorization: 'Bearer '+localStorage.getItem('pruebaSession')}}).then(response => {
      this.microposts = response;
    });
  }
});
