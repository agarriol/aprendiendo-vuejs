import template from './edit.pug';

export default Vue.extend({
  template: template(),
  components: {
  },
  data() {
    return {
      id: this.$route.params.id,
      microposts: [],
      errors: [],
      content: null,
      title:null,
      file: null
    };
  },
  methods: {
    checkForm: function(e) {
      if(!this.content && ! this.title)
      {
        this.errors = [];
        this.errors.push("Content or title required.");
        e.preventDefault();
      }else{
        const formData = new FormData();
        formData.append("micropost[picture]", this.file);
        formData.append("micropost[content]", this.content);
        formData.append("micropost[title]", this.title);
        this.sendPost(formData);
        //this.sendPost({content: this.content});
        return true;
      }
    },
    sendPost: function sendPost(data) {
      API.micropost.update(this.$route.params.id, data, {headers: {Authorization: 'Bearer '+localStorage.getItem('pruebaSession')}}).then(response => {
        this.errors.push(response);
      });
      //location.reload(true);
      //location.href ="http://localhost:8080";
    },
    processFile(event) {
      this.file = event.target.files[0];
    }
  },
  created: function () {
    API.micropost.show(this.$route.params.id, {headers: {Authorization: 'Bearer '+localStorage.getItem('pruebaSession')}}).then(response => {
      this.errors = [];
      this.microposts = response;
    });
  }
});