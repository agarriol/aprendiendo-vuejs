import {mapState} from 'vuex';
import modalComponent from 'js/components/modal/index.js';
import router from 'js/router.js';
import template from './show.pug';

export default Vue.extend({
  template: template(),
  components: {
    modalComponent
  },
  data() {
    return {
      id: this.$route.params.id,
      microposts: [],
      errors: [],
      newComment: '',
      comments: '',
      showModal: false
    };
  },
  computed: {
    currentUser() {
      return this.$store.state.users;
    }
  },
  methods: {
    checkForm: function checkForm(e) {
      this.sendPost({content_comment: this.newComment, micropost_id: this.id});
      return true;
    },
    sendPost: function sendPost(data) {
      API.comment.create(data, {headers: {Authorization: 'Bearer '+localStorage.getItem('pruebaSession')}}).then(response => {
        if (response.status === 422) {
          this.varErrorReset();
          this.errors.push(response);
          for (let i = 0; i < response.data.errors.length; i++) {
            this.errors.push(response.data.errors[i]);
            if (response.data.errors[i].title === 'Invalid content') {
              this.contentHasError = true;
            }
          }
          this.contentHasError = true;
        } else {
          this.varReset();
        }
      });
      //router.go(0);
    },
    varReset() {
      this.microposts = '';
      this.comment = '';
      this.varErrorReset();
    },
    varErrorReset() {
      this.errors = [];
      this.titleHasError = false;
      this.contentHasError = false;
    },
    deleteComment: function deleteComment(id) {
      let r = confirm('¿Quiere borrar el comentario?');
      if (r === true) {
        API.comment.destroy(id, {headers: {Authorization: 'Bearer '+localStorage.getItem('pruebaSession')}}).then(response => {
        });
        router.go(0);
      }
    },
    deleteMicropost: function deleteMicropost(){
      this.showModal = false;
      API.micropost.destroy(this.id, {headers: {Authorization: 'Bearer '+localStorage.getItem('pruebaSession')}}).then(response => {
      });
      router.go('/micropost');
    }
  },
  created: function created() {
    API.micropost.show(this.$route.params.id, {headers: {Authorization: 'Bearer '+localStorage.getItem('pruebaSession')}}).then(response => {
      this.microposts = response;
    });
    API.comment.index({headers: {Authorization: 'Bearer '+localStorage.getItem('pruebaSession')}},'?filter[micropost]='+this.id).then(response => {
      this.comments = response;
    });
  }
});
