import VueRouter from 'vue-router';

// Pages
import Home from './pages/home/index.js';
import Graficas from './pages/graficas/index.js';
import Login from './pages/login/index.js';
import Micropost from './pages/microposts/index.js';
import ShowMicropost from './pages/microposts/show.js';
import CreateMicropost from './pages/microposts/create.js';
import EditMicropost from './pages/microposts/edit.js';

// Le indicamos a Vue que use VueRouter
Vue.use(VueRouter);

// Componente vacio para usarlo en las rutas padres
const RouteParent = {render(c) { return c('router-view'); }};

// 2. Define algunas rutas
// Cada ruta debe mapear a un componente. El "componente" puede
// ser un constructor de componente creado a través de
// Vue.extend(), o simplemente un objeto de opciones de componente.
// Más tarde hablaremos acerca de las sub-rutas.
const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      layout: 'default'
    }
  },
  {
    path: '/graficas',
    name: 'graficas',
    component: Graficas,
    meta: {
      layout: 'menu'
    }
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {
      layout: 'default'
    }
  },
  {
    path: '/micropost',
    name: 'micropost',
    component: Micropost,
    meta: {
      layout: 'default'
    }
  },
  {
    path: '/micropost/show/:id',
    name: 'showMicropost',
    component: ShowMicropost,
    meta: {
      layout: 'default'
    }
  },
  {
    path: '/micropost/edit/:id',
    name: 'editMicropost',
    component: EditMicropost,
    meta: {
      layout: 'default'
    }
  },
  {
    path: '/micropost/create',
    name: 'createMicropost',
    component: CreateMicropost,
    meta: {
      layout: 'default'
    }
  },
  {
    path: '*',
    redirect: '/'
  }
];

// 3. Crea una instancia del _router_ y pasa la opción `routes`
// Puedes pasar opciones adicionales aquí,
// pero mantengámoslo simple por el momento.
const router = new VueRouter({routes});

export default router;
