import template from './index.pug';
import SidebarComponent from 'js/components/sidebar/index.js';

export default Vue.extend({
  template: template({}),
  components: {
    SidebarComponent
  },
  data() {
    return {
    };
  },
  computed: {
  },
  methods: {
  },
  created() {
  }
});
