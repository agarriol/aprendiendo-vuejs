import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    users: {
      userToken: '',
      userEmail: '',
      role: ''
    }
  },
  mutations: {
    // saveUser: token => state.userToken = token
    saveUser(state, user) {
      state.users.userEmail = user.email;
      state.users.userToken = user.jwt;
    },
    deleteUser(state) {
      state.users.userToken = '';
      state.users.userEmail = '';
    }
  },
  actions: {
    saveUser(context, user) {
      context.commit('saveUser', user);
    },
    deleteUser(context) {
      context.commit('deleteUser');
    }
  },
  getters: {
    loginUser: state => {
      return state.users;
    }
  }
});
